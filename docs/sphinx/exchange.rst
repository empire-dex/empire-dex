.. _exchangedetails:
Exchange Details
================

EmpireDEX is a decentralized exchange which allows users to exchange tokens and provide liquidity all in a trustless and non-custodial environment. This simply means that EmpireDEX does not need to own the private keys to your wallet in order to exchange tokens or provide liquidity.

EmpireDEX is an Automated Market Maker (AMM), which uses Liquidity Pairs to ensure trading can always take place. Anyone can become a liquidity provider (LP) for a pool by depositing an equivalent value of each underlying token in return for Liquidity Tokens. These tokens track pro-rata LP shares of the total reserves, and can usually be redeemed for the underlying assets at any time. The tokens in a Liquidity Pair are always standing ready to accept one token for the other as long as the “constant product” formula is preserved.

 This formula, most simply expressed as x * y = k, states that trades must not change the product (k) of a pair’s reserve balances (x and y). Because k remains unchanged from the reference frame of a trade, it is often referred to as the invariant.
 
 This formula has the desirable property that larger trades (relative to reserves) execute at exponentially worse rates than smaller ones.

The general idea of an AMM was first proposed by Vitalik Buterin, before being utilised by the now extremely popular Uniswap platform. What has followed since then in DEX's have largely been clones of the Uniswap contracts, with very little innovation occuring to improve upon the rather basic model. EmpireDEX has integrated Weaponized Liquidity into the exchange contracts themselves, allowing for any token that is deployed on EmpireDEX with **locked liquidity** to use this powerful feature in whatever way they see fit.

Trading Fees
-------------------
0.3% fee total per trade:

• 0.25% to liquidity provider for the pair being traded
• 0.05% to the `Stablecoin Vault <https://empire-dex.readthedocs.io/en/latest/liquidity_pools.html?highlight=vault#stablecoin-vault>`_


Unique to EmpireDEX
-------------------
.. _sweepableliquiditypairs:
Sweepable Liquidity Pairs
--------------------------

Projects and platforms can utilize `Weaponized Liquidity <https://empire-dex.readthedocs.io/en/latest/sweepable_pairs.html>`_ by being able to sweep one side of the liquidity pair without affecting the price of the underlying asset. 

What this ultimately does is gives projects and platforms major control over their cryptographic assets like never before. These funds ca be used however they wish.

Here are some examples of how to use your **Weaponized Liquidity**:

 | • Sweeping backing token to do buybacks 
 | • Sweeping backing token to give back to their communities (such as through staking pools)
 | • Sweeping backing token to fund initiatives like expansion (such as exchange listings) and marketing
 | • Sweeping to invest in yield generatin DeFi platforms

This now gives projects the ability to have full control over their charts and thus the value of the underlying asset. During stages of rapid growth, the price levels can be kept healthy through liquidity management in a process we like to call **Weaponized Liquidity**.

.. _timelockedliquidity:
Time Locked Initial Liquidity 
-------------------

Projects can now Time-Lock their initial liquidity from the liquidity creation and listing level from within the EmpireDEX itself, no longer having to use third-party applications such as Unicrypt and Team Finance. These locks can be set up for either a specific holder (useful for vesting team tokens) or for the entire token as a whole (meaning all users who create liquidity have it locked for the set duration, useful for preventing bank runs on the liquidity for a certain amount of time).

Projects have 4 options to choose from when it comes to Time-Locking Initial Liquidity:


• No Locks

• 3 Month Time-Lock

• 6 Month Time-Lock

• 12 Month Time-Lock

EmpireDEX focuses on providing a safe and secure environment for projects and communities. Thus giving projects no excuse as to why they havent time-locked liquidity at launch for community and investor trust and transparency. 

 Please Note that Initial Liquidity Time-Locks ONLY the initial liquidity.
 It **DOES NOT** Time-Lock the addition of more liquidity from users and the tokens communties.

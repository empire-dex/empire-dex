.. _multichainexpansion:
Multichain DEX Expansion
=========================

EmpireDEX was initially launched on BSC, however, there is a continual expansion of EmpireDEX to various networks, see below for a list of current and upcoming planned networks. Beyond the planned networks, further expansion will continue dependent on network activity and community sentiment.

All new EmpireDEX's will utilise the same router, factory and pair contracts carrying over their mechanics to each network. However, each of these chains will have their own DEX tokens and own LGE so we can re-peat the same sweepable bootstrapping mechanics to incentivise liquidity without market dilution on each chain. These DEX tokens will launch in a staggered fashion one after the other, at a planned rate of 1 network per week.

The idea here is to create a DEX multichain ecosystem between the various EmpireDEX's that will eventually be able to facilitate cross chain swaps between all the chains within Prism Network. This would mean that you could trade any token on an EmpireDEX with any other token on another chain's EmpireDEX. With the integration of EmpireDEX into exchange aggregators, this means that effeectively any token on any chain will be able to trade for any token on any connected chain.

.. _currentnetworks:
Current Networks:
------------------
- Binance Smart Chain
- Fantom
- Avalanche
- Polygon
- xDai
- Ethereum
- OKEx Chain
- Cronos

.. _plannednetworks:
Planned Networks:
------------------
Ordered by what will release first, although this is subject to change. The LGE for that chain's EMPIRE will start roughly on the day of the release of that networks 

- 18th November:  Harmony
- 25th November:  Arbitrum

Future Networks:
------------------
Blockchains we are looking into expanding to. Not ordered by release, more to be added as expansion continues. This is not a comprehensive list, as there are far more chains that we would like to expand to given time.

- Moonbeam
- Solana 
- Cardano
- Polkadot
- PulseChain
- TomoChain
- Huobi ECO Chain
- KuCoin Community Chain
- IOTA
- Tron
- Hedera
- NEAR
- Kusama
- Cosmos
- Kadena
- Velas

Binance Smart Chain:
------------
`EmpireDEX BSC <https://bsc.empiredex.org/>`_

**Binance Smart Chain**
 • Name - **EmpireDEX**
 • DEX token - **EMPIRE**
 • Initial Token Supply - **10,000**
 • Token Features - **0.1% burn on tx**
 • LP Incentives - **Earn EMPIRE, BNB & BUSD**
 • LGE - **62.5% supply / BNB from LGE** 
 • Farms - **27.5% supply over 1.5 years**

EMPIRE address

.. code-block:: bash

   0xc83859c413a6ba5ca1620cd876c7e33a232c1c34

EMPIRE/BNB LP address

.. code-block:: bash

   0xf3114cb351f38f8e8ff17f8313ff91cec4ff196f

Factory address

.. code-block:: bash

   0x06530550A48F990360DFD642d2132354A144F31d
WBNB address:

.. code-block:: bash

   0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c
Router address:

.. code-block:: bash

   0xdADaae6cDFE4FA3c35d54811087b3bC3Cd60F348

   
Escrow Contract Address:

.. code-block:: bash
 
   0x38F73653fA46943de76c228a47b7d9B81F28FFff



Fantom:
------------
`EmpireDEX Fantom <https://ftm.empiredex.org/>`_

**Fantom**
 • Name - **EmpireDEX Fantom**
 • DEX token - **FDEX**
 • Initial Token Supply - **10,000**
 • Token Features - **0.1% burn on tx**
 • LP Incentives - **Earn FDEX, FTM & USDC**
 • LGE - **52.5% supply / FTM from LGE** 
 • Farms - **27.5% supply over 1.5 years**

FDEX address

.. code-block:: bash

   0xc83859c413a6ba5ca1620cd876c7e33a232c1c34

FDEX/FTM LP address

.. code-block:: bash

   0xc12a874065b52926f89f29253744c15bc37eaf0b
Factory address

.. code-block:: bash

   0x06530550A48F990360DFD642d2132354A144F31d

WFTM address:

.. code-block:: bash

   0x21be370d5312f44cb42ce377bc9b8a0cef1a4c83

Router address:

.. code-block:: bash

   0xdADaae6cDFE4FA3c35d54811087b3bC3Cd60F348
   
Escrow Contract Address:

.. code-block:: bash
 
   0x38F73653fA46943de76c228a47b7d9B81F28FFff

Avalanche:
------------
`EmpireDEX Avalanche <https://avax.empiredex.org/>`_

**Avalanche**
 • Name - **EmpireDEX Avalanche**
 • DEX token - **ADEX**
 • Initial Token Supply - **10,000**
 • Token Features - **0.1% burn on tx**
 • LP Incentives - **Earn ADEX, AVAX & USDC**
 • LGE - **52.5% supply / AVAX from LGE** 
 • Farms - **27.5% supply over 1.5 years**

ADEX address

.. code-block:: bash

   0xc83859C413a6bA5ca1620cD876c7E33a232c1C34

ADEX/AVAX LP address

.. code-block:: bash

   0x80832376db4e414c88629C54923aEcB9855346C0
Factory address

.. code-block:: bash

   0x06530550A48F990360DFD642d2132354A144F31d

WAVAX address:

.. code-block:: bash

   0xB31f66AA3C1e785363F0875A1B74E27b85FD66c7

Router address:

.. code-block:: bash

   0xdADaae6cDFE4FA3c35d54811087b3bC3Cd60F348
   
Escrow Contract Address:

.. code-block:: bash
 
   0x38F73653fA46943de76c228a47b7d9B81F28FFff

Polygon:
------------
`EmpireDEX Polygon <https://polygon.empiredex.org/>`_

**Polygon**
 • Name - **EmpireDEX Polygon**
 • DEX token - **PDEX**
 • Initial Token Supply - **10,000**
 • Token Features - **0.1% burn on tx**
 • LP Incentives - **Earn PDEX, MATIC & USDC**
 • LGE - **52.5% supply / MATIC from LGE** 
 • Farms - **27.5% supply over 1.5 years**

PDEX address

.. code-block:: bash

   0xc83859c413a6ba5ca1620cd876c7e33a232c1c34

PDEX/MATIC LP address

.. code-block:: bash

   0x6d0d97fe6c323e5b3248ceb356dbf74708c2d8be
Factory address

.. code-block:: bash

   0x06530550A48F990360DFD642d2132354A144F31d

WMATIC address:

.. code-block:: bash

   0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270

Router address:

.. code-block:: bash

   0xB2855A6dAeeBDB72B0176A479A983066ae9775A6
   
Escrow Contract Address:

.. code-block:: bash
 
   0x38F73653fA46943de76c228a47b7d9B81F28FFff

xDai:
------------
`EmpireDEX xDai <https://xdai.empiredex.org/>`_

**xDai**
 • Name - **EmpireDEX xDai**
 • DEX token - **XDEX**
 • Initial Token Supply - **10,000**
 • Token Features - **0.1% burn on tx**
 • LP Incentives - **Earn XDEX, xDai & DAI**
 • LGE - **52.5% supply / xDai from LGE** 
 • Farms - **27.5% supply over 1.5 years**

XDEX address

.. code-block:: bash

   0xc83859C413a6bA5ca1620cD876c7E33a232c1C34

XDEX/XDAI LP address

.. code-block:: bash

   0xF6E0EeeFb7AcB6D5557d617DA1E24873997Ab3e9
Factory address

.. code-block:: bash

   0x06530550A48F990360DFD642d2132354A144F31d

WXDAI address:

.. code-block:: bash

   0xe91D153E0b41518A2Ce8Dd3D7944Fa863463a97d

Router address:

.. code-block:: bash

   0xdADaae6cDFE4FA3c35d54811087b3bC3Cd60F348
   
Escrow Contract Address:

.. code-block:: bash
 
   0x38F73653fA46943de76c228a47b7d9B81F28FFff

Ethereum:
------------
`Rootkit-EmpireDEX <https://www.rootdex.org/#/swap>`_

**Ethereum**
 • Name - **Rootkit-EmpireDEX**
 • DEX token - **ROOTDEX**
 • Initial Token Supply - **10,000**
 • Token Features - **0.1% burn on tx**
 • LP Incentives - **Earn ROOTDEX, ETH & Stablecoin**
 • LGE - **52.5% supply / ROOTDEX from LGE** 
 • Farms - **27.5% supply over 1.5 years**

ROOTDEX address

.. code-block:: bash

   0x2302f393690487a4Fc5927bBeF63ff113E0c479d

ROOTDEX/ETH LP address

.. code-block:: bash

   0x163B890f4892D593945579028Ec44Ef0f20A4633
Factory address

.. code-block:: bash

   0xd674b01E778CF43D3E6544985F893355F46A74A5

WETH address:

.. code-block:: bash

   0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2

Router address:

.. code-block:: bash

   0xe7A504316BebbE540496E29798187c9ECAD6ef4F
   
Escrow Contract Address:

.. code-block:: bash
 
   0x9fF555DDEbD500C0F1fa7C898dCbdb7CCa6809FC
OKEx Chain:
------------
`EmpireDEX OEC <https://oec.empiredex.org/#/swap>`_

**OKEx Chain**
 • Name - **EmpireDEX OEC**
 • DEX token - **OKDEX**
 • Initial Token Supply - **10,000**
 • Token Features - **0.1% burn on tx**
 • LP Incentives - **Earn OKDEX, OKT & Stablecoin**
 • LGE - **52.5% supply / OKDEX from LGE** 
 • Farms - **27.5% supply over 1.5 years**

OKDEX address

.. code-block:: bash

   0xc83859c413a6ba5ca1620cd876c7e33a232c1c34

OKDEX/OKT LP address

.. code-block:: bash

   #
Factory address

.. code-block:: bash

   #

WOKT address:

.. code-block:: bash

   #

Router address:

.. code-block:: bash

   #
   
Escrow Contract Address:

.. code-block:: bash
 
   #

Cronos Chain:
------------
`EmpireDEX Cronos <https://cro.empiredex.org/#/swap>`_

**Cronos Chain**
 • Name - **EmpireDEX Cronos**
 • DEX token - **CRODEX**
 • Initial Token Supply - **10,000**
 • Token Features - **0.1% burn on tx**
 • LP Incentives - **Earn CROKDEX, OKT & Stablecoin**
 • LGE - **52.5% supply / OKDEX from LGE** 
 • Farms - **27.5% supply over 1.5 years**

CRODEX address

.. code-block:: bash

   0xc83859C413a6bA5ca1620cD876c7E33a232c1C34

CRODEX/CRO LP address

.. code-block:: bash

   0x80523212e85e7FD850c85CC804263Ad421696d87
Factory address

.. code-block:: bash

   0x06530550A48F990360DFD642d2132354A144F31d

WCRO address:

.. code-block:: bash

   0x5C7F8A570d578ED84E63fdFA7b1eE72dEae1AE23

Router address:

.. code-block:: bash

   0xdADaae6cDFE4FA3c35d54811087b3bC3Cd60F348
   
Escrow Contract Address:

.. code-block:: bash
 
   0x9fF555DDEbD500C0F1fa7C898dCbdb7CCa6809FC

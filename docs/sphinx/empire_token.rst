EMPIRE Token
============
.. _tokenOverview:
Token Overview
-----------------
 Note: EmpireDEX was originally launched on BSC, with the associated token being under the ticker of EMPIRE. Each network that EmpireDEX is deployed on has it's own associated token with `seperate ticker names <https://empire-dex.readthedocs.io/en/latest/empire_token.html#contractAddresses>`_. As each token has the same tokenomics & structure, the information below is relevant to all but for clarities sake DEX will be referenced. In instances of DEX/COIN LP, the backing token/COIN part refers to the native token for that chains (COIN = BNB on BSC, COIN = FTM on Fantom etc.).

EMPIRE Token is the DEX token for EmpireDEX. EMPIRE is a **deflationary** fixed supply asset that boasts a revolutionary feature that we call `Weaponized Liquidity <https://empire-dex.readthedocs.io/en/latest/sweepable_pairs.html>`_. This positions EmpireDEX to use the backing tokens in it's DEX/COIN LP pairs to utilize as ammunition to bootstrap and propel the EmpireDEX protocol in unprecedented way that break the expected norms of liquidity generation on decentralized exchange protocols allowing EmpireDEX to stand freely in a market of its own.

Essentially, if every holder of DEX sold their tokens the price would go to a certain level (the price floor) and there would still be backing tokens in the LP pair. This liquidity below the price floor is normally unaccessible and to a certain extent 'wasted', but with Weaponized Liquidity EmpireDEX is able to make use of these backing tokens for a wide range of purposes such as using to seed various yield farms as additional income for liquidity providers on EmpireDEX. On top of that, DEX/COIN LP holders earn a share of the total trade volume of that network's EmpireDEX in the form of the :ref:`stablecoinvault`.

As DEX is deflationary, each trade of the token removes some of the circulating supply and thus drives the price floor up over time.

.. _tokenomics:
Tokenomics
-----------------

The maximum supply is **10,000** and no more can be minted afer launch. 

Unique features:
 | • 0.1% burn on each transaction (deflationary over time)
 | • Sweepable mechanic

.. _tokenDistrubtion:
Token Distribution
-----------------

Initial distribution of EMPIRE Token on BSC:

+-----------------+----------------+-----------+
|  Purpose        | % total supply | # tokens  |
+=================+================+===========+
| LGE Event       |      62.5      |   6,250   |
+-----------------+----------------+-----------+
| LP Rewards      |      27.5      |   2,750   |
+-----------------+----------------+-----------+
| Empire Team     |        7       |    700    |
+-----------------+----------------+-----------+
| Onniscia Team   |        3       |    300    |
+-----------------+----------------+-----------+

Initial distribution of ROOTDEX Token on Ethereum:

+-------------+----------------+-----------+
|  Purpose    | % total supply | # tokens  |
+=============+================+===========+
| LGE Event   |      52.5      |   5,250   |
+-------------+----------------+-----------+
| LP Rewards  |      27.5      |   2,750   |
+-------------+----------------+-----------+
| Empire Team |       5        |   500     |
+-------------+----------------+-----------+
| Rootkit Team|       5        |   500     |
+-------------+----------------+-----------+
| Airdrop     |       10       |   1000    |
+-------------+----------------+-----------+

Distribution of DEX Tokens on any other network:

+-------------+----------------+-----------+
|  Purpose    | % total supply | # tokens  |
+=============+================+===========+
| LGE Event   |      52.5      |   5,250   |
+-------------+----------------+-----------+
| LP Rewards  |      27.5      |   2,750   |
+-------------+----------------+-----------+
| Empire Team |       10       |   1000    |
+-------------+----------------+-----------+
| Airdrop     |       10       |   1000    |
+-------------+----------------+-----------+
 

EmpireDEX Treasury:

 Contract Address:
.. code-block:: bash

   0x5abbd94bb0561938130d83fda22e672110e12528

EmpireDEX Reserves (for liquidity pool rewards):

 Contract Address:
.. code-block:: bash

   0x3F9B7da1d832199b2dD23670F2623193636f2e88

EMPIRE Marketing:
 
 Contract address:
.. code-block:: bash

   0xA581289F88A2cC9D40ad990F5773c9e6973bc756

Development Partner (Omniscia)

 Contract address: 
.. code-block:: bash

   0x21cfe244fEe27Dcf77c9555A24075fdf0930d656

.. _contractAddresses:
Contract Addresses
-----------------
-
 EMPIRE - Binance Smart Chain:
.. code-block:: bash

    0xc83859c413a6ba5ca1620cd876c7e33a232c1c34
    
-
 FDEX - Fantom:
.. code-block:: bash

    0xc83859c413a6ba5ca1620cd876c7e33a232c1c34
-
 ADEX - Avalanche:
.. code-block:: bash

    0xc83859c413a6ba5ca1620cd876c7e33a232c1c34
-
 PDEX - Polygon:
.. code-block:: bash

    0xc83859c413a6ba5ca1620cd876c7e33a232c1c34
-
 XDEX - Polygon:
.. code-block:: bash

    0xc83859c413a6ba5ca1620cd876c7e33a232c1c34
-
 ROOTDEX - Ethereum:
.. code-block:: bash

    0x2302f393690487a4Fc5927bBeF63ff113E0c479d
-
 OKDEX - OKEx:
.. code-block:: bash

    0x
-
 CRODEX - Cronos:
.. code-block:: bash

    0x0001DF70E9cDC0C1C6b24E2172b89b105C879DDc
    



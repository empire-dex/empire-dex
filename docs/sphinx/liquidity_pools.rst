.. _liquiditypools:
Liquidity Pools
===============

Liquidity Pools simply allows users of the exchange to seamlessly swap between multiple pairs. When you provide liquidity for a Liquidity Pool, you provide a 50/50 split of both tokens in the trading pair. These tokens are then used when people buy or sell one of the tokens into the pair, with the more liquidity provided the lower the impact a trade will have.

In return these Liquidity Pool providers receive EmpireDEX liquidity provider tokens (EMP LP) that represent their share as a % of the pool.

Traders who use this pool pay a small fee (0.3%), 0.25% of which is split proportionally amongst the liquidity pool, with the remaining 0.05% selling into a stablecoin which is then claimable from the Stablecoin Vault.

.. _stablecoinvault:
Stablecoin Vault
--------------------

0.05% of every trade from every single liquidity pair on EmpireDEX is sold into a stablecoin and sent to that network's Stablecoin Vault. The stablecoin depends on the network.

The rewards in the Stablecoin Vault are **specific to DEX/COIN liquidity providers ONLY** who have permanently locked their liquidity in this pair to support the sweepable mechanis of the DEX token. Each network's trade volume goes only to the Stablecoin Vault for that network, meaning each Stablecoin Vault will have different levels of returns based on the amount of trade volume on that network's EmpireDEX as well as the total amount of DEX/COIN LP tokens held.

Rewards from the Stablecoin Vault are calculated from total amount of DEX/COIN owned. Please note that the rewards from these Stablecoin Vaults only begin to payout after 3 months to allow for volume to build up, but they are accumulating rewards the entire time since launch.

 As an example. if I have 100 DEX/COIN LP tokens 

 • I stake 80 DEX/COIN LP in a staking pool 

 • I hold 20 DEX/COIN LP in my wallet

 I am eligible to claim a share of Stablecoins from the Stablecoin Vault based off 100 DEX/COIN LP that I own. 




Liquidity Provider Rewards
---------------------------

Liquidity Providers for each pair on EmpireDEX earn a share of the 0.25% fee on all trades for the pools they provide liquidity for. This share is based on their percent ownership of the total LP tokens for that pair.


Liquidity Provider Risks
------------------------

Liquidity Providers should be aware of the inherent risks of entering a pool, such as Impermanent Loss.

Impermanent Loss increases as the prices of the two assets in the pair move away from one another. The Loss comes from the swaps into the worse performing asset as the price separation occurs. For calculating potential loss of value from Impermanent Loss, `this calculator <https://dailydefi.org/tools/impermanent-loss-calculator/>`_ can be utilised.

.. image:: images/imp_loss.png

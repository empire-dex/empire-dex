.. _sweepablepairs:

Sweepable Pairs
===============

Empire has a groundbreaking sweep mechanic on the empire pair, which we like to refer to as **Weaponized Liquidity**.

DEX/COIN LP allows one side of the liquidity pair to be swept and re-used without affecting the price of the token. 

For example, this allows EMPIRE to take COIN out of the permanently locked liquidity and reward the COIN tokens back to liquidity providers of DEX, whilst ensuring minimal market dilution for liquidity incentives. However, the applications for the swept COIN are infinite and will be used for a variety of use cases to help support the overall health and future of EmpireDEX. More information about Weaponized Liquidity can be found `here <https://medium.com/@EmpireDEX.org/what-is-weaponised-liquidity-738545d44e9f>`_.

 A quick example:

  Let's say you have 1000 BNB / 1000 XXX token in the liquidity pool.

  If you sweep 500 BNB from that pool, you will be left with 500 BNB / 1000 XXX token, which makes the pool more volatile to price movements on each buy and sell.

  You then use 300 of the BNB that you swept to buyback XXX token, therefore increasing its value against a shallower pool. Then you can input the 200 remaining BNB back into the Liquidity Pool you swept it from to increase price stability.
  
  Effectively giving projects the ability to control their charts via liquidity management and build attractive charts in ways that no other DEX can facilitate. 


For Developers
--------------

.. image:: images/dex.png

EmpireDEX provides two unique contract functions:

.. code-block:: bash

   addEmpireLiquidity()
   addEmpireLiquidityETH()


These functions allow you to pass in a deadline and pairInfo.


.. code-block:: bash

   uint256 deadline,
   PairStruct calldata pairInfo

So in essence...


3rd party projects coming onto the platform can choose **Locked** on the sweepable section and have **Weaponized Liquidity** mechanics from the liquidity creation level.

**NOTE: Only permanently locked liquidity is sweepable.**

This allows Token Creators to access a % of one side of the pair `Token A / Token B`.

Due to having tight controls around this, EmpireDEX can achieve this for Token Creators, whom can re-use it for; 

• buybacks

• building an attractive chart for traders

• marketing
• rewards for pools
• lending initiatives
• expansion of teams
• funding infrastucture 
• the possibities are endless

EmpireDEX has the potential to boostrap a new alt-season by allowing any project to list on the exchange with their very own **Weaponized Liquidity** mechanics. Giving complete freedom to projects on a liquidity management level like never before.


Video Demo
----------
`EmpireDEX Sweepable Token Demo <https://www.youtube.com/watch?v=cGnUUOnA-ok>`_

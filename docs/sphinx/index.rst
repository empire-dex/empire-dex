Empire DEX
==========

.. image:: images/image0.png


Official Documentation for EmpireDEX.

Getting Started
---------------


For information on taking part in a Liquidity Generation Event for a new network, information can be found on the :ref:`lge` page.

 Note: EmpireDEX was originally launched on BSC, with the associated token being under the ticker of EMPIRE. Each network that EmpireDEX is deployed on has it's own associated token with `seperate ticker names <https://empire-dex.readthedocs.io/en/latest/empire_token.html#contract-Addresses>`_. As each token has the same tokenomics & structure, the information throughout the documentation is relevant to all networks/Empire tokens, but for clarities sake DEX will be referenced. In instances of referring to DEX/COIN LP, COIN/backing token refers to the native token for that chains EmpireDEX (COIN = BNB on BSC, COIN = FTM on Fantom etc.). DEX = EMPIRE on BSC, FDEX on Fantom, ADEX on Avalanche etc.


Web3 Wallet Support
-------------------

* Metamask
* Trust Wallet
* WalletConnect


Key Features
--------

EmpireDEX features include:

* Non-Custodial Decentralized Exchange
* Trading / Token Swaps
* `Yield Farming <https://empire-dex.readthedocs.io/en/latest/staking_on_prism.html>`_
* `Liquidity Providing <https://empire-dex.readthedocs.io/en/latest/liquidity_pools.html#>`_ 
* `Liquidity Sweeping (Weaponised Liquidity) <https://empire-dex.readthedocs.io/en/latest/sweepable_pairs.html>`_
* `Time-Locked Initial Liquidity <https://empire-dex.readthedocs.io/en/latest/exchange.html#time-locked-initial-liquidity>`_ 
* `Stablecoin Vault <https://empire-dex.readthedocs.io/en/latest/liquidity_pools.html#busd-vault>`_
* Empire Router
* Empire Pair
* Empire Factory

Exchange Fees
-----------------

0.3% fees on swaps broken down in the following: :ref:`Liquidity Pools`

• 0.25% passively rewarded to Liquidity Providers
• 0.05% sells into a :ref:`Stablecoin Vault` for EMPIRE/COIN LP providers

Yield Farming
--------------

The highest performing liquidity pairs in the EmpireDEX will be eligible to earn rewards on `Prism Network <https://www.prismnetwork.io/>`_.

• DEX Rewards

 *Example*

 *Stake BUSD/BNB -> earn EMPIRE*
 *Stake WETH/BNB -> earn EMPIRE*

DEX LP Features
----------------------

• Permanently locked to be able to **"Weaponize Liquidity"** and sweep COIN to boostrap COIN rewards for DEX/COIN LP pair
• Exclusive Access to claim stablecoins from the Stablecoin Vault
• Highest allocation of DEX rewards in staking pools
• Yield of COIN rewards in staking pools 

License
-------

Copyright 2021 EmpireDEX under the Apache License, Version 2.0.

.. toctree::
   :hidden:
   :caption: Table of Contents

   lge
   empire_token
   exchange
   liquidity_pools
   sweepable_pairs
   multichain
   staking_on_prism
   PRISM Network <https://www.prismnetwork.io/>
   socials
   

.. _lge:

LGE
===

  Please note that DEX/COIN Coin LP tokens are **permanently wrapped** and can never be redeemed for the underlying assets, this is what makes the `Weaponised Liquidity <https://empire-dex.readthedocs.io/en/latest/sweepable_pairs.html>`_ feature possible.

  COIN = Native coin for the network you are contributing to the LGE on. So FTM on Fantom, AVAX on Avalanche etc.
  
  DEX = EmpireDEX token for that network. EMPIRE on BSC, FDEX on Fantom, ADEX on Avalanche etc.
 
LGE Specifications: 
-------------------

The Liquidity Generation Event (LGE) is a fair launch event. EmpireDEX launches are designed to be as fair as possible to all participants and use a trustless smart contract meaning at no point can funds be stolen from those who take part in the LGE. 

LGE's last for 14 days during which participants can deposit the native coin for that network. So FTM for Fantom, ETH for Ethereum, AVAX for Avalanche etc. After the LGE has finished, the raised funds are paired with 60% of the supply of Empire tokens for that network to create liquidity for DEX/COIN LP. 

**DEX/COIN LP is highly incentivised via 3 ways:**

1. Staking DEX/COIN LP has the highest allocation of **DEX** tokens in our `staking pools <https://empire-dex.readthedocs.io/en/latest/staking>`_.

2. Staking DEX/COIN LP will earn you the highest yield rewards in **COIN** token. When we use the Weaponised Liquidity feature on the DEX/COIN pair, some of the COIN swept from this is allocated to the staking pool.

3. DEX/COIN Liquidity Providers have exclusive access to earn and claim a share of the **Stablecoins** generated in the :ref:`stablecoinvault`. 

  *The Stablecoin Vault accumilates 0.05% of all trades on EmpireDEX across every tradable pair, and sells that into a stablecoin (differs for each network) which is then provided as an* **exclusive** *passive stablecoin income for DEX/COIN LP owners.*

**Token Distribution of LGE**:

DEX/COIN LP & DEX tokens are distributed proportionally to contributors to the LGE based on the amount of COIN they have input.

6,250 DEX tokens for LGE are split as follows: 
 |   • 6,000 for initial liquidity (DEX/COIN)
 |   • 250 as bonus tokens

.. image:: images/lge.png


Admin fees for LGE duration:
----------------------------

There is a 10% admin fee on all funds deposited during LGE, which is divided as: 
 | • 50% Empire Treasury
 | • 50% Marketing

**Empire Treasury** 
 | • 5% of LGE input 
 | Contract Address:
.. code-block:: bash

   0x5abbd94bb0561938130d83fda22e672110e12528

**Empire Marketing** 
 | • 5% of LGE input
 | Contract Address:
.. code-block:: bash

   0xA581289F88A2cC9D40ad990F5773c9e6973bc756

.. _lgeschedule:
LGE Schedule:
----------------------------

EmpireDEX will have a rolling series of LGE's as it continues to expand to more networks. Currently planned LGE events are as follows below:

+-----------------+-----------------+---------------+
|  Network        |       Start     |     Finish    |
+=================+=================+===============+
| Fantom          | 23rd September  | 6th October   |
+-----------------+-----------------+---------------+
| Avalanche       | 30th September  | 13th October  |
+-----------------+-----------------+---------------+
| Polygon         | 6th October     | 20th October  |
+-----------------+-----------------+---------------+
| xDai            | 13th October    | 27th October  |
+-----------------+-----------------+---------------+
| Ethereum        | 28th October    | 11th November |
+-----------------+-----------------+---------------+
| OKEx            | 4th November    | 18th November |
+-----------------+-----------------+---------------+
| Cronos          | 11th November   | 25th November |
+-----------------+-----------------+---------------+
| Harmony         | 18th November   | 2nd December  |
+-----------------+-----------------+---------------+
| Arbitrum        | 25th November   | 9th December  |
+-----------------+-----------------+---------------+

.. _snapshot:
Snapshot for Airdrop:
----------------------------
Holders of EMPIRE on BSC will be taken during each new LGE. Holders recorded in this snapshot are eligible to claim 1/10th the amount of DEX tokens for the new chain being added. This is claimable after a 6 month period. This applies to ALL EMPIRE held, including EMPIRE in your wallet, EMPIRE in LP pairs and EMPIRE in staking pools. 

This effectively means that just by holding EMPIRE, you are securing multiple airdrops across a wide variety of chains.

  For example, if you held 100 EMPIRE on BSC during the LGE for EmpireDEX Fantom, you would be eligible to claim 10 FDEX after 6 months.

Staking Pools
================
Upon launch of a new networks Empire token after the LGE, staking pools are set up to distribute 27.5% of the token supply over a period of roughly 1.5 years. Reward emissions do not begin until 24 hours after the LGE in order to give participants from all timezones the chance to pre-stake so that no participants are left at a disadvantage and miss out on the early rewards. These pools are designed to help encourage the creation of liquidity for the new network's EmpireDEX, while also distributing the token supply to the users that help contribute to the success of the project.

Staking pools have variable APYs based on:

* Total value staked in the pool
* Total value of reward tokens
* Total duration of pool

There is a 1.5% withdrawal fee for unstaking from the pools. This can be mitigated by holding PRISM on the chain you are withdrawing from. Something to note here is that this PRISM is based on what you currently hold in your wallet on that chain, meaning if you are staking on multiple chains you can get the fee reduction on all those pools by simply bridging your PRISM to the relevant chain prior to withdrawing.

* 25 PRISM = 25% fee reduction (1.125% withdrawal fee)
* 50 PRISM = 50% fee reduction (0.75% withdrawal fee)
* 100 PRISM = 100% fee reduction (0% withdrawal fee)

Boosting
-----
**What is Boosting?**

Boosting is a means to increase your effective weighted stake in a Pool. This can be done via purchasing boosts or by simply minting PRISM. Boosting does not increase the total emissions of the staked pools, but simply increase your relative weighting in the pool compared to everyone else that it is staked. As the amount you yield from staking is derived from how much % of the staked value you hold, increasing this effetive % means you will get a larger share of the rewards emitted. 

**Local Boosts**

Users can spend various tokens to increase their boost on a specific pool. Generally there are 4 different tokens to spend to boost your stake in that pool. Each of these boosts stack in combination with each other.

**Global Boosts**

Minting PRISM will give Users an additional boost that works across all networks. Currently, implmentation of this only works on the Ethereum mainnet, as this cross chain interaction is in development by our partner Chainlink. 

Let’s run through an example Pool to demonstrate how they work.


 STAKE -> 3000 REWARD (Duration 30 Days)
 
 In this Pool, users would input STAKE Tokens, and farm REWARD tokens over a period of 30 days. This means that every day ~100 REWARD tokens are distributed as rewards to stakers. 
 
 If this Pool launched, and only UserA input 1 STAKE, they would be farming 100% of these yields. If UserB joins with 1 STAKE as well, from then on both UserA and UserB will be splitting the yield 50/50 as they both own 50% of the total stake.

 If UserC was to then join with 4 STAKE, they would own 4/6 STAKE in the Pool which is ~66.66% of the total stake. This means that each day they would be earning 66.66 REWARD tokens.
 
 UserA has 1 STAKE in the Pool (~16.66%).

 UserB has 1 STAKE in the Pool (~16.66%).

 UserC has 4 STAKE in the Pool (~66.66%).
 
 UserB activates a 50% boost. This increases their weighted stake to 1.5 STAKE, meaning they now own ~23% of the Pool and will begin earning more immediately.


BSC Pools
-----

EMPIRE/BNB -> EMPIRE 
 * 120 EMPIRE per Month

EMPIRE -> EMPIRE
 * 25 EMPIRE per Month

EMPIRE/EMPIRE-BNB LP -> EMPIRE
 * 20 EMPIRE per Month

bPRISM/EMPIRE -> EMPIRE
 * 12 EMPIRE per month

BNB/BUSD -> EMPIRE
 * 10 EMPIRE per Month

BNB/ETH -> EMPIRE
 * 10 EMPIRE per Month

ETH/BUSD -> EMPIRE
 * 10 EMPIRE per Month

BNB/USDT -> EMPIRE
 * 10 EMPIRE per Month

bPRISM/BNB -> EMPIRE
 * 15 EMPIRE per Month

Fantom Pools
-----

FDEX/FTM -> FDEX
 * 90 FDEX per Month

FDEX/fPRISM -> FDEX
 * 25 FDEX per Month

FDEX/FDEX-FTM LP -> FDEX
 * 25 FDEX per Month

FTM/fPRISM -> FDEX
 * 35 FDEX per Month

FTM/USDC -> FDEX
 * 15 FDEX per Month

FTM/ETH -> FDEX
 * 15 FDEX per Month

FTM/BTC -> FDEX
 * 10 FDEX per Month

FDEX -> FDEX
 * 20 FDEX per Month

fPRISM -> FDEX
 * 10 FDEX per Month

Avalanche Pools
-----
ADEX/AVAX -> ADEX
 * 120 ADEX per Month

ADEX/aPRISM -> ADEX
 * 30 ADEX per Month

ADEX/ADEX-AVAX LP -> ADEX
 * 30 ADEX per Month

AVAX/aPRISM -> ADEX
 * 30 ADEX per Month

ADEX -> ADEX
 * 25 ADEX per Month

aPRISM -> ADEX
 * 15 ADEX per Month

Polygon Pools
-----
PDEX/MATIC -> PDEX
 * 120 PDEX per Month

PDEX/pPRISM -> PDEX
 * 30 PDEX per Month

PDEX/PDEX-MATIC LP -> PDEX
 * 30 PDEX per Month

MATIC/pPRISM -> PDEX
 * 30 PDEX per Month

PDEX -> PDEX
 * 25 PDEX per Month

pPRISM -> PDEX
 * 15 PDEX per Month

xDai Pools
-----
XDEX/XDAI -> XDEX
 * 120 XDEX per Month

XDEX/xPRISM -> XDEX
 * 30 XDEX per Month

XDEX/XDEX-XDAI LP -> XDEX
 * 30 XDEX per Month

XDAI/xPRISM -> XDEX
 * 30 XDEX per Month

XDEX -> XDEX
 * 25 XDEX per Month

xPRISM -> XDEX
 * 15 XDEX per Month

Ethereum Pools
-----
ROOTDEX/ETH -> ROOTDEX
 * # ROOTDEX per Month

ROOTDEX/PRISM -> ROOTDEX
 * # ROOTDEX per Month

ROOTDEX/ROOTDEX-ETH LP -> ROOTDEX
 * # ROOTDEX per Month

ETH/PRISM -> ROOTDEX
 * # ROOTDEX per Month

ROOTDEX -> ROOTDEX
 * # ROOTDEX per Month

PRISM -> ROOTDEX
 * # ROOTDEX per Month

OKEx Pools
-----
OKDEX/OKT -> OKDEX
 * # OKDEX per Month

OKDEX/okPRISM -> OKDEX
 * # OKDEX per Month

OKDEX/OKDEX-OKT LP -> OKDEX
 * # OKDEX per Month

OKT/okPRISM -> OKDEX
 * # OKDEX per Month

OKDEX -> OKDEX
 * # OKDEX per Month

okPRISM -> OKDEX
 * # OKDEX per Month

Cronos Pools
-----
EMPIRE/CRO -> EMPIRE
 * 160 EMPIRE per Month

EMPIRE/croPRISM -> EMPIRE
 * 25 EMPIRE per Month

EMPIRE/EMPIRE-CRO LP -> EMPIRE
 * 30 EMPIRE per Month

CRO/croPRISM -> EMPIRE
 * 15 EMPIRE per Month

CRO/ETH -> EMPIRE
 * 10 EMPIRE per Month

CRO/USDC -> EMPIRE
 * 10 EMPIRE per Month

EMPIRE -> EMPIRE
 * 45 EMPIRE per Month
